﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        LinkedList<int> list = new LinkedList<int>();
        FillList(list);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        InsertionSort(list);

        stopwatch.Stop();
        long duration = stopwatch.ElapsedTicks * (1000000000L / Stopwatch.Frequency);

        Console.WriteLine("Time for sorting: " + duration + " ns");
    }

    static void FillList(LinkedList<int> list)
    {
        int[] allowedSizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        Console.WriteLine("Enter the number of elements (choose from 10, 100, 500, 1000, 2000, 5000, 10000):");
        int numElements;
        while (true)
        {
            if (int.TryParse(Console.ReadLine(), out numElements) && Array.Exists(allowedSizes, size => size == numElements))
            {
                break;
            }
            Console.WriteLine("Invalid input. Please enter a valid number from the list:");
        }

        Random rand = new Random();
        for (int i = 0; i < numElements; i++)
        {
            list.AddLast(rand.Next(0, 10000));
        }
    }

    static void InsertionSort(LinkedList<int> list)
    {
        LinkedListNode<int> current = list.First?.Next;

        while (current != null)
        {
            LinkedListNode<int> key = current;
            LinkedListNode<int> prev = current.Previous;
            while (prev != null && prev.Value > key.Value)
            {
                int temp = prev.Value;
                prev.Value = key.Value;
                key.Value = temp;
                key = prev;
                prev = prev.Previous;
            }
            current = current.Next;
        }
    }
}
