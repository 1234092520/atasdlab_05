﻿using System;
using System.Collections.Generic;

public class Node<T>
{
    public T Data { get; set; }
    public Node<T> Next { get; set; }
    public Node<T> Prev { get; set; }

    public Node(T data)
    {
        Data = data;
        Next = null;
        Prev = null;
    }
}

public class Linked<T>
{
    public Node<T> Head { get; set; }

    public Linked()
    {
        Head = new Node<T>(default(T));
    }

    public void AddToEnd(T data)
    {
        Node<T> newNode = new Node<T>(data);
        Node<T> temp = Head;
        while (temp.Next != null)
        {
            temp = temp.Next;
        }
        temp.Next = newNode;
        newNode.Prev = temp;
    }

    public void PrintList()
    {
        Node<T> temp = Head.Next;
        while (temp != null)
        {
            Console.Write(temp.Data + " ");
            temp = temp.Next;
        }
        Console.WriteLine();
    }
}

class Program
{
    static Linked<int> linkedList = new Linked<int>();

    static void Main(string[] args)
    {
        FillList(linkedList);
        Console.Write("Default list: ");
        linkedList.PrintList();
        InsertionSort(linkedList);
        Console.Write("Sorted list: ");
        linkedList.PrintList();
    }

    static void FillList(Linked<int> list)
    {
        Random rand = new Random();
        for (int i = 0; i < 10; ++i)
        {
            int randomNumber = rand.Next(-10, 11);
            list.AddToEnd(randomNumber);
        }
    }

    static void InsertionSort(Linked<int> list)
    {
        for (Node<int> i = list.Head.Next; i != null; i = i.Next)
        {
            int key = i.Data;
            Node<int> j = i.Prev;
            while (j != null && j.Data > key)
            {
                j.Next.Data = j.Data;
                j = j.Prev;
            }
            if (j == null)
                list.Head.Data = key;
            else
                j.Next.Data = key;
        }
    }
}
