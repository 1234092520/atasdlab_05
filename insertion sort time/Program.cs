﻿using System;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        int[] array = new int[0];
        FillArray(ref array);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        InsertionSort(array);

        stopwatch.Stop();
        long duration = stopwatch.ElapsedTicks * (1000000000L / Stopwatch.Frequency);

        Console.WriteLine("Time for sorting: " + duration + " ns");
    }

    static void FillArray(ref int[] array)
    {
        int[] allowedSizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        Console.WriteLine("Enter the number of elements (choose from 10, 100, 500, 1000, 2000, 5000, 10000):");
        int numElements;
        while (true)
        {
            if (int.TryParse(Console.ReadLine(), out numElements) && Array.Exists(allowedSizes, size => size == numElements))
            {
                break;
            }
            Console.WriteLine("Invalid input. Please enter a valid number from the list:");
        }

        Random rand = new Random();
        array = new int[numElements];
        for (int i = 0; i < numElements; i++)
        {
            array[i] = rand.Next(0, 10000);
        }
    }

    static void InsertionSort(int[] array)
    {
        for (int i = 1; i < array.Length; i++)
        {
            int key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > key)
            {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }
}
