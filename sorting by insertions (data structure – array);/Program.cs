﻿using System;

class Program
{
    const int arraySize = 10;
    static int[] myArray = new int[arraySize];

    static void Main(string[] args)
    {
        FillArray(myArray);
        Console.Write("Default array: ");
        PrintArray(myArray);
        InsertionSort(myArray);
        Console.Write("Sorted array: ");
        PrintArray(myArray);
    }

    static void FillArray(int[] array)
    {
        Random rand = new Random();
        for (int i = 0; i < arraySize; ++i)
        {
            int randomNumber = rand.Next(-10, 11);
            array[i] = randomNumber;
        }
    }

    static void PrintArray(int[] array)
    {
        foreach (int num in array)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine();
    }

    static void InsertionSort(int[] array)
    {
        for (int i = 1; i < arraySize; ++i)
        {
            int key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > key)
            {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }
}
