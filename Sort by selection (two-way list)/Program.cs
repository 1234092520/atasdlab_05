﻿using System;
using System.Diagnostics;

class Node
{
    public int Data;
    public Node Prev, Next;
    public Node(int data)
    {
        Data = data;
        Prev = Next = null;
    }
}

class DoublyLinkedList
{
    private Node head;

    public void AddLast(int data)
    {
        Node newNode = new Node(data);
        if (head == null)
        {
            head = newNode;
        }
        else
        {
            Node temp = head;
            while (temp.Next != null)
            {
                temp = temp.Next;
            }
            temp.Next = newNode;
            newNode.Prev = temp;
        }
    }

    public void SelectionSort()
    {
        for (Node i = head; i != null; i = i.Next)
        {
            Node min = i;
            for (Node j = i.Next; j != null; j = j.Next)
            {
                if (j.Data < min.Data)
                {
                    min = j;
                }
            }
            int temp = i.Data;
            i.Data = min.Data;
            min.Data = temp;
        }
    }

    public void PrintList()
    {
        Node temp = head;
        while (temp != null)
        {
            Console.Write(temp.Data + " ");
            temp = temp.Next;
        }
        Console.WriteLine();
    }
}

class Program
{
    static void Main(string[] args)
    {
        DoublyLinkedList list = new DoublyLinkedList();
        Random rand = new Random();
        int n = 10; 
        for (int i = 0; i < n; i++)
        {
            list.AddLast(rand.Next(1, 100));
        }

        Console.WriteLine("Original list:");
        list.PrintList();

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        list.SelectionSort();

        stopwatch.Stop();
        Console.WriteLine("Sorted list:");
        list.PrintList();

        Console.WriteLine("Time taken: " + stopwatch.ElapsedMilliseconds + " ms");
    }
}
