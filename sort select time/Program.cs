﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        LinkedList<int> list = new LinkedList<int>();
        FillList(list);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        SelectionSort(list);

        stopwatch.Stop();
        long duration = stopwatch.ElapsedTicks * (1000000000L / Stopwatch.Frequency);

        Console.WriteLine("Time for sorting: " + duration + " ns");
    }

    static void FillList(LinkedList<int> list)
    {
        int[] allowedSizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        Console.WriteLine("Enter the number of elements (choose from 10, 100, 500, 1000, 2000, 5000, 10000):");
        int numElements;
        while (true)
        {
            if (int.TryParse(Console.ReadLine(), out numElements) && Array.Exists(allowedSizes, size => size == numElements))
            {
                break;
            }
            Console.WriteLine("Invalid input. Please enter a valid number from the list:");
        }

        Random rand = new Random();
        for (int i = 0; i < numElements; i++)
        {
            list.AddLast(rand.Next(0, 10000));
        }
    }

    static void SelectionSort(LinkedList<int> list)
    {
        LinkedListNode<int> current = list.First;

        while (current != null)
        {
            LinkedListNode<int> minNode = current;
            LinkedListNode<int> next = current.Next;

            while (next != null)
            {
                if (next.Value < minNode.Value)
                {
                    minNode = next;
                }
                next = next.Next;
            }

            int temp = current.Value;
            current.Value = minNode.Value;
            minNode.Value = temp;

            current = current.Next;
        }
    }
}
